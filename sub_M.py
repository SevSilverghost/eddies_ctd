# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
#import seaborn as sns
from mpl_toolkits.basemap import Basemap
#========================================
from math import radians, sin, cos, sqrt, asin, degrees, pow
from pylab import *
##
from scipy.interpolate import griddata

import matplotlib.pyplot as plt
import numpy.ma as ma
from scipy import interpolate
import time
import scipy as sp
import scipy.stats
from datetime import timedelta, date
import numpy.ma as ma

#%%
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

def check_nans(in_list):
    i = 0
    for a in in_list:
        if str(float(a))=='nan':
            i+=1
    return i
#%%
Eddy_type = 'CE'

CTD_Dict = dict()
mat_contents = sio.loadmat('3D_CTD_CE_data.mat',mdict=CTD_Dict)

#MEAN_Dict = dict()
#mat_mean_contents = sio.loadmat('T_daily_CTD_MEAN.mat',mdict=MEAN_Dict)
#%%
start = time.time()

CTD_Slice = list()
DX_list = list()

plt.close()
plt.hexbin(CTD_Dict['DX'][0],CTD_Dict['DY'][0],vmax=20, vmin=0)
#plt.ylim(-1.5, 1.5)
#plt.xlim(-1.5, 1.5)
plt.colorbar()
plt.show()

#%%


zi = np.arange(0,2005,5)

#%%
# loop over all profiles
for dind,dd in enumerate(CTD_Dict['DAYS'][0]):
    # check if profile lies within 0.1R from section "Y=0"
    if -0.5<=CTD_Dict['DX'][0][dind]<=+0.5:
        
        if check_nans(CTD_Dict['SAL'][dind])<200:
            #save DX position
            DX_list.append(float(CTD_Dict['DY'][0][dind]))
            #interpolate S profile
            S_new = CTD_Dict['SAL'][dind]
        
        #for pos,item in enumerate(S_new):
        #    if str(item)=='nan':
        #        S_new[pos] = 1000
        #        
        #S_new = ma.masked_values(S_new,1000)
        #S_new = S_new.filled(S_new.mean())
        
        #f = interpolate.interp1d(MEAN_Dict['ZIND'], S_new,assume_sorted=True, bounds_error=False,fill_value=np.mean(S_new),kind='linear')
        #S_new = f(MEAN_Dict['ZIND'])
        
        #extract mean profile from DB
        #CTD_yday = (date.fromordinal(int(dd)) + timedelta(days=int(dd)%1) - timedelta(days = 366)).timetuple().tm_yday
        #Mean_S_profile = MEAN_Dict['TEMP'][CTD_yday-1]
        
        #interpolate S_mean profile
        #S_mean_new = ma.masked_array(Mean_S_profile)
        #f = interpolate.interp1d(MEAN_Dict['ZIND'], S_mean_new,assume_sorted=True, bounds_error=False,fill_value=np.mean(S_mean_new),kind='linear')
        #S_mean_new = f(MEAN_Dict['ZIND'])

        
        #subtract
        #Diff_profile=ma.masked_array(S_mean_new)
            Diff_profile=ma.masked_array(S_new)
        #if str(S_new[0])=='nan':
        #    print len(S_new)
        #for pos,S in enumerate(S_new):
            #Diff_profile.append(float(S_mean_new[pos]-S))

            
            CTD_Slice.append(ma.masked_array(Diff_profile))
        
        #CTD_Slice = ma.masked_array(CTD_Slice)







end = time.time()
print(end - start)
#%%
CTD_Slice = np.array(CTD_Slice)


#%%
#DX_list = [-0.5,0.1,0.5]
#CTD_Slice = [[1,2,3,1,2,5],[8,10,3,1,2,5],[1,2,3,1,20,15]]
#CTD_Slice = np.transpose(CTD_Slice)
#%%

#DXT = list(DX_list)
#DHT = list(np.arange(0,401))
xv, hv = meshgrid(DX_list, zi)

#%%
xi = np.linspace(-2.0,2.0,100)
hi = np.linspace(0.0,2000.0,500)

#%%

zi_T = griddata((xv.flatten(), hv.flatten('F')), CTD_Slice.flatten(), (xi[None,:], hi[:,None]), method='nearest')

#%%
plt.close()
mylevels = list(np.arange(5.0,20.0,0.1))
#CS = plt.contourf(CTD_Slice,aspect='auto',origin='lower',extent=[-2,2,0,2000],cmap=plt.cm.RdBu_r)
#CS = plt.contourf(zi_T,levels=mylevels,aspect='auto',origin='lower',extent=[-2,2,0,2000],cmap=plt.cm.RdBu_r)
#CS2 = plt.contour(zi_T,20,aspect='auto',origin='lower',extent=[-2,2,0,2000],interpolation='linear',color='k')
CS = plt.imshow(zi_T,aspect='auto',origin='lower',extent=[-2,2,0,2000],cmap=plt.cm.RdBu_r,interpolation='nearest')
plt.ylim(20, 300)
#plt.xlim(-1.5, 1.5)
plt.gca().invert_yaxis()
plt.colorbar()
plt.title('S along-y section over the '+Eddy_type+' center')
plt.show()
#%%

#%%

#%%
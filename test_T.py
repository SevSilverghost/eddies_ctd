# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 16:37:54 2016

@author: andrei

test onpe ARGO model climat profiles
"""

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
#import seaborn as sns
from mpl_toolkits.basemap import Basemap
#========================================
from math import radians, sin, cos, sqrt, asin, degrees, pow
from pylab import *
##
from scipy.interpolate import griddata

import matplotlib.pyplot as plt
import numpy.ma as ma
from scipy import interpolate
import time
import scipy as sp
import scipy.stats
from datetime import timedelta, date
import numpy.ma as ma

#%%
def find_CLIM_profile(LONG,LAT):
    for ind,val in enumerate(Data):
        if float(val['LONG'])==float(LONG) and float(val['LAT'])==float(LAT):
            return int(ind)
    
    return int(100000)
#%%
Eddy_type = 'CE'
ARGO_Dict = dict()
mat_contents = sio.loadmat('3D_ARGO_'+Eddy_type+'_data.mat',mdict=ARGO_Dict)
#%%

A_LONGS = list(float("%0.4f"%float(x)) for x in ARGO_Dict['LONG'][0])
A_LATS = list(float("%0.4f"%float(x)) for x in ARGO_Dict['LAT'][0])

#%%
ARGO_climate_file = open('CLIMPROFILE-T-1.txt','r')

Data = list()
for line in ARGO_climate_file:
    d = line.split(',')
    c_prof = list(d[3:])
    if float(d[1]) in A_LONGS and float(d[2]) in A_LATS:
        Data.append(dict(LONG=float(d[1]),LAT=float(d[2]),TEMP=list(c_prof)))
    #print line.split(',')[0]




#%%
CTD_Slice = []
DX_list = []
for dind,dd in enumerate(ARGO_Dict['DAYS'][0]):
    # check if profile lies within 0.5R from section "X=0"
    if -0.5<=ARGO_Dict['DX'][0][dind]<=+0.5:
        if check_nans(ARGO_Dict['TEMP'][dind])<200:
            # find climate profile
            ARGO_LAT = "%0.4f"%float(ARGO_Dict['LAT'][0][dind])
            ARGO_LONG = "%0.4f"%float(ARGO_Dict['LONG'][0][dind])
            
            c_pos = find_CLIM_profile(float(ARGO_LONG),float(ARGO_LAT))
            if c_pos<100000:
                S_clim = np.array(Data[c_pos]['TEMP'],dtype='float32')
            else:
                print 'warning! unable to locate suitable climate profile!'
                print ARGO_LONG,ARGO_LAT
            #save DX position
            DX_list.append(float(ARGO_Dict['DY'][0][dind]))
            #interpolate S profile
            S_new = np.array(ARGO_Dict['TEMP'][dind],dtype='float32')
            
            #
            #subtract
            Diff_profile=ma.masked_array(S_clim-S_new)
            CTD_Slice.append(ma.masked_array(Diff_profile))




#%%
CTD_Slice = np.array(CTD_Slice)


#%%
#DX_list = [-0.5,0.1,0.5]
#CTD_Slice = [[1,2,3,1,2,5],[8,10,3,1,2,5],[1,2,3,1,20,15]]
#CTD_Slice = np.transpose(CTD_Slice)
#%%

#DXT = list(DX_list)
#DHT = list(np.arange(0,401))
xv, hv = meshgrid(DX_list, zi)

#%%
xi = np.linspace(-2.0,2.0,100)
hi = np.linspace(1.0,1601.0,400)

#%%

zi_T = griddata((xv.flatten(), hv.flatten('F')), CTD_Slice.flatten(), (xi[None,:], hi[:,None]), method='linear')

#%%
plt.close()
mylevels = list(np.arange(-5.5,5.5,0.05))
#CS = plt.contourf(CTD_Slice,aspect='auto',origin='lower',extent=[-2,2,0,2000],cmap=plt.cm.RdBu_r)
CS = plt.contourf(zi_T,levels=mylevels,aspect='auto',origin='lower',extent=[-2,2,0,2000],cmap=plt.cm.RdBu_r)
#CS2 = plt.contour(zi_T,20,aspect='auto',origin='lower',extent=[-2,2,0,2000],interpolation='linear',color='k')
#CS = plt.imshow(zi_T,aspect='auto',origin='lower',extent=[-2,2,1,1601],cmap=plt.cm.RdBu_r,interpolation='nearest')
plt.ylim(20, 300)
#plt.xlim(-1.5, 1.5)
plt.gca().invert_yaxis()
plt.colorbar()
plt.title('$' + Eddy_type + '\ Anomaly \ T_{clim} - T_{ARGO} \ section \ X=0 $')
plt.show()






#%%







#%%







#%%







#%%





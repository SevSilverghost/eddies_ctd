# -*- coding: utf-8 -*-
"""
Spyder Editor

"""

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
#import seaborn as sns
from mpl_toolkits.basemap import Basemap
#========================================
from math import radians, sin, cos, sqrt, asin, degrees, pow
from pylab import *
##
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import numpy.ma as ma
from scipy import interpolate
from datetime import timedelta, date
import time
#%%
def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

#%%
DictCTD = dict()
mat_contents = sio.loadmat('3D_CTD_CE_data.mat',mdict=DictCTD)

#%%
sio.whosmat('3D_CTD_AE_data.mat')
#%%

days = list(int(round(float(x))) for x in DictCTD['DAYS'][0])

#%%
start = time.time()
CTD_file = open('CTD_locations_CE.dat','w')

for pos,cday in enumerate(days):
    CTD_file.write("%04d"%(pos+1)+' ')
    
    CTD_file.write("%03d"%(date.fromordinal(cday) + timedelta(days=cday%1) - timedelta(days = 366)).timetuple().tm_yday+' ')
    CTD_file.write("%0.4f"%float(DictCTD['LONG'][0][pos])+' ')
    CTD_file.write("%0.4f"%float(DictCTD['LAT'][0][pos])+' ')
    
    CTD_file.write("%6d"%(cday)+' ')
    CTD_file.write('\n')
    #ARGO_file.write(' '.join(list("%0.4f"%float(x[pos]) for x in MyDict['ti'])))
    #ARGO_file.write('\n')
    #print len(list(float(x[pos]) for x in MyDict['ti']))
    #print len(list(float(x[pos]) for x in MyDict['si']))

CTD_file.flush()
CTD_file.close()
end = time.time()
print(end - start)
#%%
print "%03d"%9
#%%

#%%

#%%